"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("./server/server");
const guests_router_1 = require("./guests/guests.router");
const hotels_router_1 = require("./hotels/hotels.router");
const server = new server_1.Server();
server.bootstrap([guests_router_1.guestRouter, hotels_router_1.hotelRouter]).then(server => {
    console.log('Server is listening on: ', server.application.address());
}).catch(error => {
    console.log('Server failed to start');
    console.error(error);
    process.exit(1);
});
