import * as mongoose from 'mongoose'
const validarCpf = require('validar-cpf')

export interface Guest extends mongoose.Document {
    name: string,
    email: string,
    gender: string,
    cpf: string
}

const guestSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 80,
        minlength: 3
    }, 
    email: {
        type: String,
        unique: true,
        match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        required: true
    },
    gender: {
        type: String,
        required: false,
        enum: ['F', 'M']
    },
    cpf: {
        type: String,
        required: false,
        validate: {
            validator: validarCpf,
            message: '{PATH}: Invalid CPF ({VALUE})'
        }
    }
})

export const Guest = mongoose.model<Guest>('Guest', guestSchema)