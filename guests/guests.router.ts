import {Router} from '../common/router'
import {Guest} from './guests.model'
import {NotFoundError} from 'restify-errors'
import * as restify from 'restify'

class GuestRouter extends Router {

    applyRoutes(application: restify.Server) {
        application.get('/guests', (req, res, next) => {
            Guest.find().then(this.render(res, next)).catch(next)
        })

        application.get('/guests/:id', (req, res, next) => {
            Guest.findById(req.params.id).then(this.render(res, next)).catch(next)
        })

        application.post('/guests', (req, res, next) => {
            let user = new Guest(req.body)
            user.save().then(this.render(res, next)).catch(next)
        })

        application.put('/guests/:id', (req, res, next) => {
            const options = { overwrite : true }
            Guest.update({_id: req.params.id}, req.body, options).exec().then(result => {
                if (result.n) {
                    return Guest.findById(req.params.id)
                }
                else {
                    throw new NotFoundError('Document not found')
                }
            }).then(this.render(res, next)).catch(next)
        })

        application.patch('/guests/:id', (req, res, next) => {
            const options = {new : true}
            Guest.findByIdAndUpdate(req.params.id, req.body, options).then(this.render(res, next))
        })

        application.del('/guests/:id', (req, res, next) => {
            Guest.remove({_id: req.params.id}).exec().then(cmdResult => {
                if (cmdResult.n) {
                    res.send(204)
                }
                else {
                    throw new NotFoundError('Document not found')
                }
                return next()
            }).catch(next)
        })
    }
}

export const guestRouter = new GuestRouter()