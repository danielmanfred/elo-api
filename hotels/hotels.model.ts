import * as mongoose from 'mongoose'

export interface Address extends mongoose.Document {
    neighborhood: string,
    street: string,
    number: string,
    adjunct: string,
    zipcode: string,
    city: string,
    state: string,
    country: string
}

export interface User extends mongoose.Document {
    name: string,
    email: string,
    password: string,
    dateRegister: Date,
    level: string
}

export interface Accommodation extends mongoose.Document {
    name: string,
    description: string,
    price: {
        mon: number,
        tue: number,
        wed: number,
        thu: number,
        fri: number,
        sat: number,
        sun: number
    }
    capacity: number,
    expiration: number
}

export interface Room extends mongoose.Document {
    label: string,
    accommodation: Accommodation,
    status: boolean
}

export interface Booking extends mongoose.Document {
    room: Room,
    checkin: Date,
    checkout: Date,
    priceBooking: number,
    priceTotal: number,
    guestBooking: Guest,
    guests: Guest[],
    bookingChannel: BookingChannel,
    isPaid: boolean,
    dateRegister: Date,
    dateUpdate: Date,
    comments: string,
    adults: number,
    kids: number,
    currentPaid: number,
    paymentMethod: [{
        value: number,
        method: string
    }],
    status: string,
    orders: [{
        product: Product,
        amount: number
    }],
    estimatedTimeOfArrival : Date
}

export interface BookingChannel extends mongoose.Document {
    name: string,
    color: string
}

export interface Guest extends mongoose.Document {
    name: string,
    email: string,
    gender: string,
    dob: string,
    document: {
        docNumber: string,
        docType: string
    },
    phone: string[],
    news: boolean,
    address: Address,
    dateRegister: Date,
    dateUpdate: Date
}

export interface Product extends mongoose.Document {
    name: string,
    description: string,
    price: number
}

export interface Hotel extends mongoose.Document {
    name: string,
    email: string,
    phones: string[],
    address: Address,
    description: string,
    rules: string,
    bookingMessage: string, 
    dateRegister: Date,
    pics: string[],
    mainPic: string,
    logo: string,
    amenities: string[],
    owner: User,
    employees: User[],
    products: Product[],
    bookings: Booking[]
}

const addressShema = new mongoose.Schema({
    neighborhood: {
        type: String
    }, 
    street: {
        type: String
    },
    number: {
        type: String
    },
    adjunct: {
        type: String
    }, 
    zipcode: {
        type: String
    },
    city: {
        type: String,
        required: true,
        maxlength: 50
    },
    state: {
        type: String
    },
    country: {
        type: String
    }
})

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 80,
        minlength: 3
    }, 
    email: {
        type: String,
        unique: true,
        match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        required: false
    },
    password: {
        type: String,
        select: false,
        required: true
    },
    dateRegister: {
        type: Date,
        required: false
    },
    level: {
        type: String,
        required: false,
        enum: ['Owner', 'Admin']
    }
})

const accommodationSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    price: {
        mon: {
            type: Number
        },
        tue: {
            type: Number
        },
        wed: {
            type: Number
        },
        thu: {
            type: Number
        },
        fri: {
            type: Number
        },
        sat: {
            type: Number
        },
        sun: {
            type: Number
        }
    },
    capacity: {
        type: Number
    },
    expiration: {
        type: Number
    }
})

const roomSchema = new mongoose.Schema({
    label: {
        type: String,
        required: true
    },
    accommodation: {
        type: accommodationSchema,
        required: true
    },
    status: {
        type: Boolean
    }
})

const guestSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 80,
        minlength: 3
    }, 
    email: {
        type: String,
        unique: true,
        match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        required: true
    },
    gender: {
        type: String,
        required: false,
        enum: ['F', 'M']
    },
    dob: {
        type: Date,
        required: true,
    },
    document: {
        docNumber: {
            type: String,
            required: true
        },
        docType: {
            type: String,
            required: true,
            enum: ['cpf', 'rg', 'passport', 'cnh']
        }
    },
    phone: {
        type: [String]
    },
    news: {
        type: Boolean
    },
    address: {
        type: addressShema,
    },
    dataRegister: {
        type: Date,
        required: false
    },
    dateUpdate: {
        type: Date,
        
    }
})

const bookingChannelSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    color: {
        type: String
    }
})

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    price: {
        type: Number,
        required: true
    }
})

const bookingSchema = new mongoose.Schema({
    room: {
        type: roomSchema,
        required: true
    },
    checkin: {
        type: Date
    },
    checkout: {
        type: Date
    },
    priceBooking: {
        type: Number
    },
    priceTotal: {
        type: Number
    },
    guestBooking: {
        type: guestSchema
    },
    guests: {
        type: [guestSchema]
    },
    bookingChannel: {
        type: bookingChannelSchema
    },
    dateRegister: {
        type: Date
    },
    dateUpdate: {
        type: Date
    },
    comments: {
        type: String
    },
    adults: {
        type: Number
    },
    kids: {
        type: Number
    },
    isPaid: {
        type: Boolean
    },
    currentPaid: {
        type: Number
    },
    paymentMethod: [{
        value: {
            type: Number
        },
        method: {
            type: String
        }
    }],
    status: {
        type: String,
        enum: ['P', 'C', 'A', 'E'] // pendente, cancelado, ativo, encerrado
    },
    orders: [{
        product: {
            type: productSchema
        },
        amount: {
            type: Number
        }
    }],
    estimatedTimeOfArrival: {
        type: Date
    }
})

const hotelSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String
    }, 
    phones: {
        type: [String]
    },
    address: {
        type: addressShema,
    },
    description: {
        type: String
    },
    rules: {
        type: String
    },
    bookingMessage: {
        type: String
    },
    dataRegister: {
        type: Date,
        required: false
    },
    pics: {
        type: [String]
    },
    mainPic: {
        type: String
    },
    logo: {
        type: String
    },
    amenities: {
        type: [String]
    },
    owner: {
        type: userSchema,
        required: true
    },
    employees: {
        type: [userSchema],
        required: false,
        default: []
    },
    products: {
        type: [productSchema],
        required: false,
        select: false,
        default: []
    },
    booking: {
        type: [bookingSchema],
        required: false,
        default: []
    }
})

export const Hotel = mongoose.model<Hotel>('Hotel', hotelSchema)